#include <stm32l4xx_hal.h>
#include <stm32l4xx_hal_tim.h>
#include <stm32l4xx_hal_exti.h>
//#include <stm32l4xx_syscfg.h>
#include "string.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h> 

volatile int answer=0, first=1, read=1, wave1=1; 
volatile int EXTI_ticks=0,Send_ticks=0, change=0, new_fr=0, cheak=0; 
volatile float period,period_check;
volatile float tim_clock=20000;
TIM_HandleTypeDef htim3,htim2; 
GPIO_InitTypeDef GPIO_InitStruct; 
TIM_IC_InitTypeDef sConfigIC; 
TIM_MasterConfigTypeDef sMasterConfig; 
TIM_ClockConfigTypeDef sClockSourceConfig; 
EXTI_ConfigTypeDef  EXTI_InitStructure;
EXTI_HandleTypeDef EXTI_HandleStructure;

int frequancy(int time);

int main(void)
{

//EXTI_ConfigTypeDef EXTI_InitStruct;


// Random things

RCC_OscInitTypeDef RCC_OscInitStruct;
RCC_ClkInitTypeDef RCC_ClkInitStruct;
/*
RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSE|RCC_OSCILLATORTYPE_MSI;
RCC_OscInitStruct.LSEState = RCC_LSE_ON;
RCC_OscInitStruct.MSIState = RCC_MSI_ON;
RCC_OscInitStruct.MSICalibrationValue = 0;
RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
HAL_RCC_OscConfig(&RCC_OscInitStruct);
*/
/*

RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_MSI;
RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0);
*/
// LED
/*
__HAL_RCC_GPIOA_CLK_ENABLE();
GPIO_InitStruct.Pin = GPIO_PIN_5;
GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
GPIO_InitStruct.Pull = GPIO_NOPULL;
HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
*/

// Interupt pin (input)
__HAL_RCC_GPIOA_CLK_ENABLE();
GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
GPIO_InitStruct.Pull=GPIO_NOPULL;
GPIO_InitStruct.Pin=GPIO_PIN_0;
GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

/*
EXTI_InitStructure.Line = EXTI_LINE_0;
EXTI_InitStructure.Mode = EXTI_MODE_INTERRUPT;
EXTI_InitStructure.Trigger = EXTI_TRIGGER_RISING;
EXTI_InitStructure.GPIOSel = EXTI_GPIOA;

EXTI_HandleStructure.Line=EXTI_LINE_0;
//EXTI_HandleStructure.PendingCallback = EXTI_Pen
HAL_EXTI_GetConfigLine(&EXTI_HandleStructure,&EXTI_InitStructure);
*/


// Interupt pin (output)
__HAL_RCC_GPIOC_CLK_ENABLE();
GPIO_InitStruct.Pin = GPIO_PIN_2;
GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
GPIO_InitStruct.Speed=GPIO_SPEED_LOW;
GPIO_InitStruct.Pull=GPIO_NOPULL;
HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);


__HAL_RCC_TIM2_CLK_ENABLE();
htim2.Instance = TIM2;
htim2.Init.Prescaler = 2-1;
htim2.Init.Period = 100-1;
htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
__HAL_TIM_ENABLE_IT(&htim2, TIM_DIER_UIE);
HAL_TIM_Base_Init(&htim2);




HAL_NVIC_SetPriority(TIM2_IRQn, 0, 0);
HAL_NVIC_EnableIRQ(TIM2_IRQn);
HAL_TIM_Base_Start(&htim2);

HAL_NVIC_SetPriority(EXTI0_IRQn, 1, 0);
HAL_NVIC_EnableIRQ(EXTI0_IRQn);

volatile int final_period=0, time=0, j=0;
volatile float delta=0;
while(1)
{
if (answer==1)
   {
    period_check=period;
    final_period=frequancy(time);
    answer=0;
    Send_ticks=0;
    cheak=1;

    while (1)
    {
    //delta=abs((1/(period_check/tim_clock*2))-(1/(period/tim_clock*2)));
    //delta=abs((period_check/tim_clock*2)-(period/tim_clock*2));
    delta=abs(period_check-period);
    if(delta>1 && delta<1500)
    {
      break;
    }
  
    if(Send_ticks>=final_period/2)
    {
    change=1;
    Send_ticks=0;
    }
    read=1;
    }
  
    time++;
    if(time>=6) time=0;
   }
   
 }

}
void TIM2_IRQHandler(void)
{
    EXTI_ticks++;
    Send_ticks++;
    if(change==1)
    {
      HAL_GPIO_TogglePin(GPIOC,GPIO_PIN_2);
      change=0;
    }
      if (__HAL_TIM_GET_FLAG(&htim2, TIM_FLAG_UPDATE) != RESET)
  {
      if (__HAL_TIM_GET_IT_SOURCE(&htim2, TIM_IT_UPDATE) != RESET)
    {
        __HAL_TIM_CLEAR_IT(&htim2, TIM_IT_UPDATE);
    }
  }
}
void EXTI0_IRQHandler(void)
{
  if(wave1==0)
    {
      //HAL_GPIO_TogglePin(GPIOC,GPIO_PIN_2);
      
        if(first==1)
        {
          EXTI_ticks=0;
          first=0;
        }
        else
        {
          first=1;
          answer=1;
          period=EXTI_ticks;
        }
      }
    
  wave1=0;
    if (__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_0)!= RESET)
  {
        __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_0); // Clears The Interrupt Flag
        HAL_GPIO_EXTI_Callback(GPIO_PIN_0);   // Calls The ISR Handler CallBack Function
  }
}
int frequancy(int time)
{
  volatile double final_period;
  volatile double fr,fr_period;
  int fr_int;
  fr_period=(period_check*2);
  fr=ceil(1/(fr_period/(tim_clock)));
  

  switch(time)
  {
    case 0: 
    final_period=fr_period;
    break;
    case 1: 
    final_period=round((1/(fr+50))*tim_clock);
    break;
    case 2: 
    final_period=round((1/pow(fr,1.05))*tim_clock);
    break;
    case 3:
    final_period=round(1/(fr/(M_PI/2))*tim_clock);
    break;
    case 4:
    fr_int=fr;
    fr_int=fr_int>>1;
    fr=fr_int;
    final_period=round((1/fr)*tim_clock);
    break;
    case 5:
    final_period=round((1/(fr/(1.61803398875/2)))*tim_clock);

    
  }
  return final_period;
  
}